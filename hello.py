from flask import Flask, redirect, request, url_for

app = Flask(__name__)


@app.route("/")
def hello():
    return "Hello!"


@app.route("/work")
def working():
    return "This is working"


@app.route("/<name>")
def hi(name):
    return f"Hello {name}!"


@app.route("/work/<int:postID>")
def show(postID):
    return f"Blag Number {postID}"


@app.route("/admin")
def addy():
    return "Hello Admin!"


@app.route("/guest/<guest>")
def hello_guest(guest):
    return f"Hello {guest}"


@app.route("/user/<name>")
def hello_user(name):
    if name == "admin":
        return redirect(url_for("addy"))
    else:
        return redirect(url_for("hello_guest", guest=name))


@app.route("/success/<name>")
def success(name):
    return f"Welcome {name}"


@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "POST":
        user = request.form["nm"]
        return redirect(url_for("success", name=user))
    else:
        user = request.args.get("nm")
        return redirect(url_for("success", name=user))


@app.route("/house")
def house():
    return "<html><body><h1>Hello World</h1></body></html>"


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
