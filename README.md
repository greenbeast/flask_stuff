# How to launch these scripts

You can either just run `python hello.py` after `cd`ing into the directory or you can go in and run `login.html` by opening it in a browserand putting in a name into the textbox there. The `@app.route()` stuff is the different urls for the function below.

The `url` needed to see stuff is `localhost:5000` so just open a browser and put that in while the `hello.py` script is running.